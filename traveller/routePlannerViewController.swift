//
//  routePlannerViewController.swift
//  traveller
//
//  Created by ruby on 28/8/2017.
//  Copyright © 2017 ruby. All rights reserved.
//

import UIKit

class routePlannerViewController: UIViewController {
    /*@IBOutlet weak var nostack: UILabel!
    @IBOutlet weak var detail2: UILabel!
    @IBOutlet weak var btn: UIButton!*/
    @IBOutlet weak var titleTF: TravellerTextField!
    @IBOutlet weak var bDateTF: TravellerTextField!
    @IBOutlet weak var eDateTF: TravellerTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Main.sharedInstance.currentViewController = self
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
        
        // Do any additional setup after loading the view.
        /*titleTF.placeholder = ""
        titleTF.addBorderBottom(height: 1.0, color: UIColor.Teal)
        dateTF.placeholder = ""
        dateTF.addBorderBottom(height: 1.0, color: UIColor.Teal)*/
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func tfDatePick(_ sender: TravellerTextField) {
        let datePickerView:UIDatePicker = UIDatePicker()
        
        datePickerView.datePickerMode = .date
        datePickerView.tag = sender.tag
        
        sender.inputView = datePickerView
        sender.addToolBar()
        
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged(sender:)), for: .valueChanged)
    }
    
    func datePickerValueChanged(sender:UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateStyle = DateFormatter.Style.medium
        dateFormatter.timeStyle = DateFormatter.Style.none
        
        let tmpTF = self.view.viewWithTag(sender.tag) as? UITextField

        tmpTF?.text = dateFormatter.string(from: sender.date)
        
    }

    /*@IBAction func btnAction(_ sender: UIButton) {
        let shouldHideDetail2 = sender.titleLabel!.text! == "Hide"
        updateDetailView(hideDetail2: shouldHideDetail2, animated: true)
    }
    
    func updateDetailView(hideDetail2 shouldHideDetail2: Bool, animated: Bool) {
        let newButtonTitle = shouldHideDetail2 ? "Show" : "Hide"
        btn.setTitle(newButtonTitle, for: UIControlState())
        if animated {
            UIView.animate(withDuration: 0.3) {
                self.detail2.isHidden = shouldHideDetail2
                self.nostack.isHidden = shouldHideDetail2
            }
        }else{
            self.detail2.isHidden = shouldHideDetail2
            self.nostack.isHidden = shouldHideDetail2
        }
    }*/

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
