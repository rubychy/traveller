//
//  travellerTextField.swift
//  
//
//  Created by ruby on 6/9/2017.
//
//

import Foundation
import UIKit

class TravellerTextField: UITextField {
    let defaultColor = BasicSetting.defaultColor
    
    open override func draw(_ rect: CGRect) {
        self.addBorderBottom(height: 1.0, color: defaultColor)
    }
    
    func addBorderBottom(height: CGFloat, color: UIColor) {
        let border = CALayer()
        border.frame = CGRect(x: 0, y: self.frame.size.height - height, width: self.frame.size.width, height: height)
        border.backgroundColor = color.cgColor
        self.layer.addSublayer(border)
    }
    
    func addToolBar () {
        let tmpViewController = Main.sharedInstance.currentViewController
        
        let toolBar = UIToolbar(frame: CGRect(x: 0, y: (tmpViewController?.view.frame.size.height)!/6, width: (tmpViewController?.view.frame.size.width)!, height: 40.0))
        toolBar.layer.position = CGPoint(x: (tmpViewController?.view.frame.size.width)!/2, y: (tmpViewController?.view.frame.size.height)!-20.0)
        toolBar.barStyle = UIBarStyle.blackTranslucent
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.white
        toolBar.backgroundColor = defaultColor
        
        
        let todayBtn = UIBarButtonItem(title: "Today", style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.tappedToolBarBtn))
        let okBarBtn = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(self.donePressed))
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: self, action: nil)
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: (tmpViewController?.view.frame.size.width)! / 3, height: (tmpViewController?.view.frame.size.height)!))
        
        label.font = UIFont(name: "Helvetica", size: 12)
        label.backgroundColor = UIColor.clear
        label.textColor = UIColor.white
        label.text = "Select a due date"
        label.textAlignment = NSTextAlignment.center
        
        let textBtn = UIBarButtonItem(customView: label)
        toolBar.setItems([todayBtn,flexSpace,textBtn,flexSpace,okBarBtn], animated: true)
        
        self.inputAccessoryView = toolBar
        
    }
    
    func donePressed(_ sender: UIBarButtonItem) {
        self.resignFirstResponder()
    }
    
    func tappedToolBarBtn(_ sender: UIBarButtonItem) {
        
        let dateformatter = DateFormatter()
        dateformatter.dateStyle = DateFormatter.Style.medium
        dateformatter.timeStyle = DateFormatter.Style.none
        
        self.text = dateformatter.string(from: Date())
        self.resignFirstResponder()
    }


}
